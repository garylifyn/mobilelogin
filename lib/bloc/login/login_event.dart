part of 'login_bloc.dart';

abstract class LoginEvent extends Equatable {
  const LoginEvent();

  @override
  List<Object> get props => [];
}

class GetUser extends LoginEvent {
  final String countryCode;
  final String phoneNumber;

  const GetUser(this.countryCode, this.phoneNumber);

  @override
  List<Object> get props => [countryCode, phoneNumber];

  @override
  String toString() => 'GetUser { phone:$countryCode $phoneNumber }';
}

class SwitchAccount extends LoginEvent {
  @override
  String toString() => 'SwitchAccount';

  @override
  List<Object> get props => [];
}

class SendSMS extends LoginEvent {
  final String phone;

  SendSMS(this.phone);

  @override
  String toString() => 'SendSMS.';

  @override
  List<Object> get props => [phone];
}

class VerifyPhone extends LoginEvent {
  // final String phoneNumber;
  final AuthCredential authCredential;
  final String smsCode;

  VerifyPhone({this.authCredential, this.smsCode});

  @override
  String toString() => 'VerifyPhone';

  @override
  List<Object> get props => [authCredential, smsCode];
}

class UpdateUserDetails extends LoginEvent {
  final String name;
  final String email;
  final String country;

  UpdateUserDetails({this.name, this.email, this.country});

  @override
  String toString() =>
      'UpdateUserDetails: {name: $name, email: $email, country: $country}';

  @override
  List<Object> get props => [name, email, country];
}

class UpdateUserWithGoogle extends LoginEvent {
  @override
  String toString() => 'UpdateUserWithGoogle';

  @override
  List<Object> get props => [];
}

class UpdateUserWithFacebook extends LoginEvent {
  @override
  String toString() => 'UpdateUserWithFacebook';

  @override
  List<Object> get props => [];
}
