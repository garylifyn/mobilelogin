part of 'login_bloc.dart';

abstract class LoginState extends Equatable {
  const LoginState();
}

class LoginInitial extends LoginState {
  String toString() => 'LoginInitial';

  @override
  List<Object> get props => [];
}

class LoginLoading extends LoginState {
  String toString() => 'LoginLoading';

  @override
  List<Object> get props => [];
}

class LoginSuccess extends LoginState {
  final String name;
  final String profilePicUrl;

  const LoginSuccess(this.name, this.profilePicUrl);

  String toString() => 'LoginSuccess';

  @override
  List<Object> get props => [name, profilePicUrl];
}

class LoginFailed extends LoginState {
  final String error;

  LoginFailed(this.error);

  String toString() => 'LoginFailed {error: $error}';

  @override
  List<Object> get props => [error];
}

class VerifyPhoneInProgress extends LoginState {
  // final String phoneNumber;

  VerifyPhoneInProgress();

  String toString() => 'VerifyPhoneInProgress';

  @override
  List<Object> get props => [];
}

class SMSSent extends LoginState {
  SMSSent();

  String toString() => 'SMSSent.';

  @override
  List<Object> get props => [];
}

class UpdateUserInfo extends LoginState {
  String toString() => 'UpdateUserInfo';

  @override
  List<Object> get props => [];
}

class VerifyPhoneError extends LoginState {
  final String error;

  VerifyPhoneError(this.error);

  String toString() => 'VerifyPhoneError {error: $error}';

  @override
  List<Object> get props => [error];
}

class UpdateUserInProgress extends LoginState {
  String toString() => 'UpdateUserInProgress';
  @override
  List<Object> get props => [];
}

class UpdateUserSuccess extends LoginState {
  // final String

  String toString() => 'UpdateUserSuccess';

  @override
  List<Object> get props => [];
}

class UpdateUserFailed extends LoginState {
  final String error;

  UpdateUserFailed(this.error);

  String toString() => 'UpdateUserFailed: {error: $error}';

  @override
  List<Object> get props => [error];
}
