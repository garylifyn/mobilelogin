import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:mobilelogin/bloc/login/login_bloc.dart';

import 'package:mobilelogin/data/user_repository.dart';
import 'package:mobilelogin/screens/first_ui/firstUI.dart';

class SimpleBlocObserver extends BlocObserver {
  @override
  void onEvent(Bloc bloc, Object event) {
    super.onEvent(bloc, event);
    print(event);
  }

  @override
  void onTransition(Bloc bloc, Transition transition) {
    super.onTransition(bloc, transition);
    print(transition);
  }

  @override
  void onError(Cubit bloc, Object error, StackTrace stackTrace) {
    super.onError(bloc, error, stackTrace);
    print(error);
  }
}

void main() {
  Bloc.observer = SimpleBlocObserver();

  runApp(
    MaterialApp(
      initialRoute: '/',
      routes: {
        '/': (_) => BlocProvider(
              create: (context) => LoginBloc(UserRepository()),
              child: FirstUI(),
            )
      },
    ),
  );
}
