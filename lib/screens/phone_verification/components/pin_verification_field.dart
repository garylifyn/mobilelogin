import 'package:flutter/material.dart';
import 'package:mobilelogin/constants.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class PinVerificationField extends StatefulWidget {
  final TextEditingController smsCodeController;
  PinVerificationField({Key key, this.smsCodeController}) : super(key: key);

  @override
  _PinVerificationFieldState createState() => _PinVerificationFieldState();
}

class _PinVerificationFieldState extends State<PinVerificationField> {
  @override
  Widget build(BuildContext context) {
    return PinCodeTextField(
      length: 6,
      autoDismissKeyboard: true,
      // enableActiveFill: true,
      pinTheme: PinTheme(
        activeColor: kPrimaryColor,
        inactiveColor: Colors.grey[800],
      ),
      controller: widget.smsCodeController,
      onChanged: (value) {
        // print(value);
        // widget.smsCodeController.text = value;
      },
    );
  }
}
