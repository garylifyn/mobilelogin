import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:mobilelogin/bloc/login/login_bloc.dart';
import 'package:mobilelogin/constants.dart';
import 'package:mobilelogin/screens/phone_verification/phone_verification.dart';

import 'package:mobilelogin/screens/secondUI.dart';

import 'components/title_layout.dart';
import 'components/my_custom_form.dart';

// Globals
// final _formKey = GlobalKey<FormState>();

class FirstUI extends StatefulWidget {
  FirstUI({Key key}) : super(key: key);

  @override
  _FirstUIState createState() => _FirstUIState();
}

class _FirstUIState extends State<FirstUI> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);

    return BlocConsumer<LoginBloc, LoginState>(
      listener: (context, state) {
        if (state is LoginSuccess) {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (_) => BlocProvider.value(
                value: BlocProvider.of<LoginBloc>(context),
                child: SecondUI(),
              ),
            ),
          );
        }

        if (state is SMSSent) {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (_) => BlocProvider.value(
                value: BlocProvider.of<LoginBloc>(context),
                child: PhoneVerification(),
              ),
            ),
          );
        }
      },
      builder: (context, state) {
        if (state is LoginLoading) {
          return kLoadingWidget();
        }

        // if (state is SMSSent) {
        //   return PhoneVerification(state.phoneNumber);
        // }

        return initialLayout(context, _formKey);
      },
    );
  }
}

Widget initialLayout(BuildContext context, GlobalKey<FormState> formKey) {
  final LoginBloc _loginBloc = context.bloc<LoginBloc>();

  Size size = MediaQuery.of(context).size;

  return Scaffold(
    resizeToAvoidBottomInset: false,
    // Background Image
    body: Container(
      constraints: BoxConstraints.expand(),
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/03.png'),
          fit: BoxFit.cover,
        ),
      ),
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            TitleLayout(),
            Container(
              height: size.height / 1.5,
              padding: EdgeInsets.symmetric(
                  horizontal: size.height / 20, vertical: size.width / 9),
              decoration: BoxDecoration(
                color: Color(0xFF01B9B4),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight: Radius.circular(40),
                ),
              ),
              child: Form(
                key: formKey,
                child: MyCustomForm(
                  formKey: formKey,
                  loginBloc: _loginBloc,
                ),
              ),
            ),
          ],
        ),
      ),
    ),
  );
}

// Widget buildLoading() {
//   return Center(
//     child: CircularProgressIndicator(),
//   );
// }
