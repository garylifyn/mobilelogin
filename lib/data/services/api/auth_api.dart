import 'dart:async';

import 'package:dio/dio.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/services.dart';
import 'package:mobilelogin/utils/keys.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';

class AuthApi {
  final Dio dio = Dio();
  SharedPreferences sharedPreferences;

  final url = Keys.localhostUrl;

  String getJWT() {
    return sharedPreferences.getString('token');
  }

  Future signInWithGoogle() async {
    try {
      String token = this.getJWT();

      // Trigger the authentication flow
      final GoogleSignInAccount googleUser = await GoogleSignIn().signIn();

      // Obtain the auth details from the request
      final GoogleSignInAuthentication googleAuth =
          await googleUser.authentication;

      // Create a new credential
      final AuthCredential credential = GoogleAuthProvider.getCredential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );

      dio.options.headers = {'authorization': token};

      final body = {
        'google_access_token': googleAuth.accessToken,
      };

      // print('access token: ' + googleAuth.accessToken);
      // print('idToken: ' + googleAuth.idToken);

      final response =
          await dio.post(url + '/user/updateUserWithGoogle', data: body);

      final data = response.data;

      print(data);

      // Once signed in, return the UserCredential
      await FirebaseAuth.instance.signInWithCredential(credential);

      return data;
    } on PlatformException catch (err) {
      print(err.message);
    }
  }

  // Sign In With Facebook
  final FirebaseAuth _auth = FirebaseAuth.instance;
  Future signInWithFacebook() async {
    String token = this.getJWT();
    final facebookLogin = new FacebookLogin();
    final facebookAuth = await facebookLogin.logIn(['email']);
    final fbToken = facebookAuth.accessToken.token;

    switch (facebookAuth.status) {
      case FacebookLoginStatus.loggedIn:
        {
          final facebookCredential =
              FacebookAuthProvider.getCredential(accessToken: fbToken);
          _auth.signInWithCredential(facebookCredential);

          print(FacebookLoginStatus);
          print('facebookCredential: $facebookCredential');

          dio.options.headers = {'authorization': token};

          final body = {
            'facebook_access_token': fbToken,
          };

          final response =
              await dio.post(url + '/user/updateUserWithFacebook', data: body);

          final data = response.data;

          print(data);
          return data;
        }
        break;
      case FacebookLoginStatus.cancelledByUser:
        {
          print(FacebookLoginStatus.cancelledByUser);
          return FacebookLoginStatus.cancelledByUser;
        }
        break;
      case FacebookLoginStatus.error:
        {
          print(FacebookLoginStatus.error);
          throw FacebookLoginStatus.error;
        }
    }
  }

  Future phoneLogin(String phoneNumber) async {
    try {
      final response = await dio.post(
        url + '/user/checkUserPhone',
        data: {'phone_number': phoneNumber},
      );

      Map data = response.data;

      print('data: ${data.toString()}');

      if (data['data']['name'] == null) {
        return data['phone_number'];
      } else {
        return data;
      }
    } catch (err) {
      throw 'No phone number exist';
    }
  }

  Future sendIdToken(token) async {
    sharedPreferences = await SharedPreferences.getInstance();

    try {
      final response = await dio.post(
        url + '/user/login',
        data: {'idToken': token},
      );

      Map data = response.data;

      sharedPreferences.setString("token", data['data']['token']);

      return data;
    } catch (err) {
      print(err);
    }
  }

  Future updateUser(String name, String email, String phoneNum) async {
    // String token = sharedPreferences.getString('token');
    String token = this.getJWT();

    dio.options.headers = {'authorization': token};

    Map body = {
      'name': name,
      'email': email,
      'phone_number': phoneNum,
    };

    try {
      final response = await dio.post(
        url + '/user/update',
        data: body,
      );

      Map data = response.data;

      print('${data.toString()}');
      return data;
    } catch (err) {
      print(err.toString());
      return err;
    }
  }
}
