import 'package:equatable/equatable.dart';

class User extends Equatable {
  final int userId;
  final String name;
  final String email;
  final String phoneNumber;
  final String profilePictureUrl;

  User({
    this.userId,
    this.name,
    this.email,
    this.phoneNumber,
    this.profilePictureUrl,
  });

  User.fromJSON(Map<String, dynamic> parsedJson)
      : userId = parsedJson['data']['user_id'],
        name = parsedJson['data']['name'],
        email = parsedJson['data']['email'],
        phoneNumber = parsedJson['data']['phone_number'],
        profilePictureUrl = parsedJson['data']['profile_picture_url'];

  @override
  List<Object> get props => [userId, name, phoneNumber, profilePictureUrl];
}
